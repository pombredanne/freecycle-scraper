#!/usr/bin/env python

import requests
import scraperwiki
import lxml.html

URL = 'http://groups.freecycle.org/LiverpoolfreecycleNetwork/posts/search'
LISTING_TR = '//table//tr'

def main():
    response = requests.post(URL, params={
        'resultsperpage': 100,
        'search_words': '',
        'include_offers': 'on',
        'date_start': '',
        'date_end': ''})
    response.raise_for_status()
    root = lxml.html.fromstring(response.content)
    for tr in root.xpath(LISTING_TR):
        url = tr.xpath('./td/a/@href')[0]
        description = tr.xpath('./td/strong/a/text()')[0]
        scraperwiki.sqlite.save(['url'],
            {'url': url,
             'description': description,
             'network': 'LiverpoolfreecycleNetwork'})

if __name__ == '__main__':
    main()
